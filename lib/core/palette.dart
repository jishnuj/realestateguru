import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const primaryColor = Color(0xff006aa9);
const secondaryColor = Color(0xff191919);
const teritaryColor = Color(0xff2d2d2d);
const primaryTextColor = Color(0xff446e7a);
const secondaryTextColor = Color(0xff626060);
