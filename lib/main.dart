import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:realestateguru/core/palette.dart';
import 'package:realestateguru/presentation/pages/ask_question.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.landscapeLeft,
    DeviceOrientation.landscapeRight,
    DeviceOrientation.portraitDown,
    DeviceOrientation.portraitUp
  ]).then((_) {
    runApp(MyApp());
  });
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'RealEstateGuru',
      theme: ThemeData(
        // is not restarted.
        primaryColor: primaryColor,
      ),
      home: AskQuestionPage(),
    );
  }
}
