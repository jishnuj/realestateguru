import 'package:flutter/material.dart';
import 'package:realestateguru/core/palette.dart';
import 'package:realestateguru/presentation/pages/about_page.dart';
import 'package:realestateguru/presentation/pages/ask_question.dart';
import 'package:realestateguru/presentation/pages/home_page.dart';

class MenuDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Drawer(
        child: Container(
          color: secondaryColor,
          child: ListView(
            children: <Widget>[
              Container(
                  color: teritaryColor,
                  padding:
                      EdgeInsets.symmetric(vertical: 20.0, horizontal: 17.0),
                  child: Text('Menu',
                      style: Theme.of(context)
                          .textTheme
                          .headline5
                          .copyWith(color: secondaryTextColor))),
              SizedBox(height: 15),
                 _buildMenuOption(context, 'Ask the Guru', Icons.info, () {
                                Navigator.pop(context);

                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return AskQuestionPage();
                }));
              }),
              SizedBox(height: 10),
              _buildMenuOption(
                  context, 'Real Estate Questions', Icons.question_answer, () {
                            Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) {
                  return HomePage();
                }));
              }),
              SizedBox(height: 10),
           
              _buildMenuOption(context, 'About The Guru', Icons.person, () {
                        Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return AboutPage();
                }));
              }),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildMenuOption(
      BuildContext context, String name, IconData icon, Function onTap) {
    return ListTile(
      leading: Icon(
        icon,
        color: secondaryTextColor,
      ),
      title: Text(name,
          style: Theme.of(context)
              .textTheme
              .headline6
              .copyWith(color: secondaryTextColor)),
      onTap: () {
        onTap();
      },
    );
  }
}
