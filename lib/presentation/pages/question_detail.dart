import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:realestateguru/api/mocks/mock_service.dart';
import 'package:realestateguru/api/mocks/models/question.dart';
import 'package:realestateguru/core/palette.dart';

class QuestionDetailPage extends StatelessWidget {
  final int qid;
  const QuestionDetailPage({Key key, this.qid}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Question question = MockService().fetchQuestionById(qid);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Question"),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.all(15.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    question.question,
                    style: GoogleFonts.lato(
                      textStyle: TextStyle(
                          color: primaryTextColor,
                          letterSpacing: .5,
                          fontSize: 25.0),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Submitted by ${question.publishedBy} -${question.datePublished}",
                    style: Theme.of(context).textTheme.bodyText1.copyWith(
                          color: secondaryTextColor,
                        ),
                  ),
                ],
              ),
            ),
            Image.asset(question.imagePath),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Text(question.answer,
                  style: GoogleFonts.lato(
                      textStyle: TextStyle(
                          color: secondaryTextColor,
                          letterSpacing: .5,
                          height: 1.5,
                          fontSize: 16.0))),
            )
          ],
        ),
      ),
    );
  }
}
