import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:realestateguru/api/mocks/mock_service.dart';
import 'package:realestateguru/api/mocks/models/about.dart';
import 'package:realestateguru/core/palette.dart';

class AboutPage extends StatelessWidget {
  const AboutPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    About about = MockService().getAppABout();
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("About The Guru"),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image.asset("assets/images/phil.png"),
              SizedBox(
                height: 25.0,
              ),
              RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(children: [
                    TextSpan(
                      text:
                          """Licensed Real Estate Broker 949.249.2020\nPhil@RealEstateGuru.com\nhttp://realestateguru.com/\n""",
                      style: GoogleFonts.lato(
                          textStyle: TextStyle(
                              color: secondaryTextColor,
                              letterSpacing: .5,
                              height: 1.5,
                              fontSize: 16.0)),
                    ),
                    TextSpan(
                        text: "DRE# 01862270",
                        style: GoogleFonts.lato(
                            textStyle: TextStyle(
                                color: secondaryTextColor,
                                fontWeight: FontWeight.bold,
                                letterSpacing: .5,
                                height: 1.5,
                                fontSize: 16.0)))
                  ])),
              SizedBox(
                height: 20,
              ),
              Container(
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(width: 1, color: Colors.grey[200]))),
              ),
              Text(about.heading1),
              Text(
                about.aboutSection1,
                style: GoogleFonts.lato(
                    textStyle: TextStyle(
                        color: secondaryTextColor,
                        letterSpacing: .5,
                        height: 1.5,
                        fontSize: 16.0)),
              ),
              SizedBox(
                height: 15.0,
              ),
              Text(
                about.heading2,
                style: Theme.of(context).textTheme.headline5,
              ),
              Text(
                about.aboutSection2,
                style: GoogleFonts.lato(
                    textStyle: TextStyle(
                        color: secondaryTextColor,
                        letterSpacing: .5,
                        height: 1.5,
                        fontSize: 16.0)),
              )
            ],
          ),
        ),
      ),
    );
  }
}
