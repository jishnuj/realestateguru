import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:realestateguru/api/mocks/mock_service.dart';
import 'package:realestateguru/api/mocks/models/question.dart';
import 'package:realestateguru/core/palette.dart';
import 'package:realestateguru/presentation/pages/question_detail.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Question> questions = MockService().getAllQuestions();
    return Scaffold(
      // drawer: MenuDrawer(),
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(icon: Icon(Icons.arrow_back_ios),onPressed: (){
          Navigator.pop(context, false);
        },),
        title: Column(children: [
          Text("Real Estate Questions"),
          Text("Answered Previously by The Guru" , style: Theme.of(context).textTheme.bodyText2.copyWith(color:Colors.white,fontSize: 10),),
        ]),
      ),
      body: DoubleBackToCloseApp(
          snackBar: const SnackBar(
            content: Text('Tap back again to exit'),
          ),
          child: Container(
            child: ListView.builder(
                itemCount: questions.length,
                itemBuilder: (context, index) {
                  return _buildQuestionCard(questions, index, context);
                }),
          )),
    );
  }

  Widget _buildQuestionCard(
      List<Question> questions, int index, BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (context) {
          return QuestionDetailPage(
            qid: questions[index].id,
          );
        }));
      },
      child: Container(
        margin: const EdgeInsets.all(5.0),
        decoration: BoxDecoration(
            border: Border(bottom: BorderSide(color: Colors.grey[300]))),
        padding: const EdgeInsets.all(20.0),
        child: Text(
          questions[index].question,
          style: GoogleFonts.lato(
            textStyle: TextStyle(
                color: primaryTextColor, letterSpacing: .5, fontSize: 20.0),
          ),
        ),
      ),
    );
  }
}
