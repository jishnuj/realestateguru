import 'package:flutter/material.dart';
import 'package:mailer/mailer.dart';
import 'package:mailer/smtp_server/gmail.dart';
import 'package:realestateguru/core/palette.dart';
import 'package:realestateguru/presentation/widgets/menu_drawer.dart';

class AskQuestionPage extends StatefulWidget {
  AskQuestionPage({Key key}) : super(key: key);

  @override
  _AskQuestionPageState createState() => _AskQuestionPageState();
}

class _AskQuestionPageState extends State<AskQuestionPage> {
  TextEditingController emailController = new TextEditingController();
  TextEditingController subjectController = new TextEditingController();
  TextEditingController msgController = new TextEditingController();

  bool showLoading = false;

  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MenuDrawer(),
      appBar: AppBar(
        centerTitle: true,
        title: Column(
          children: [
            Text("Ask Questions"),
            Text("Ask The Guru Any Question",
                style: Theme.of(context)
                    .textTheme
                    .bodyText2
                    .copyWith(color: Colors.white, fontSize: 10))
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: showLoading
            ? Container(
                height: MediaQuery.of(context).size.height - 100,
                child: Center(child: CircularProgressIndicator()))
            : Container(
                // height: MediaQuery.of(context).size.height - 100,
                alignment: Alignment.center,
                child: Center(
                  child: Container(
                    // height: MediaQuery.of(context).size.height - 100,
                    padding: const EdgeInsets.all(15.0),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Image.asset("assets/images/phil.png"),
                          SizedBox(
                            height: 25.0,
                          ),
                          TextFormField(
                            controller: emailController,
                            validator: (value) {
                              if (value == null ||
                                  value.isEmpty ||
                                  !RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                      .hasMatch(value)) {
                                return 'Please enter a valid email';
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              labelText: "Email",
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(color: primaryColor)),
                            ),
                          ),
                          SizedBox(
                            height: 25.0,
                          ),
                          TextFormField(
                            controller: subjectController,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Please enter a subject';
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              labelText: "Subject",
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(color: primaryColor)),
                            ),
                          ),
                          SizedBox(
                            height: 25.0,
                          ),
                          TextFormField(
                            controller: msgController,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Please enter a question.';
                              }
                              return null;
                            },
                            maxLines: 10,
                            decoration: InputDecoration(
                              hintText: "Question",
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(color: primaryColor)),
                            ),
                          ),
                          SizedBox(
                            height: 25.0,
                          ),
                          ElevatedButton(
                              child: Text("SUBMIT"),
                              onPressed: () async {
                                if (_formKey.currentState.validate()) {
                                  setState(() {
                                    this.showLoading = true;
                                  });
                                  await sendMail(
                                      emailController.text,
                                      subjectController.text,
                                      msgController.text);
                                  ScaffoldMessenger.of(context)
                                      .showSnackBar(SnackBar(
                                    content: Text("Message Sent Successfully"),
                                  ));
                                  Navigator.pop(context);
                                }
                              })
                        ],
                      ),
                    ),
                  ),
                ),
              ),
      ),
    );
  }

  Future<void> sendMail(
      String recipeintEmail, String subject, String msg) async {
    String username = 'smsoldg@gmail.com';
    String password = '5e9uR0#851';
 
    final smtpServer = gmail(username, password);
    // Create our message.
    final message = Message()
      ..from = Address(username, 'RealEstateGuru')
      // TEST Email comment on release
      // ..recipients.add("testme221@gmail.com")
      // client Email  UNCOMMENT on release
      ..recipients.add("Phil@realestateguru.com")
      ..bccRecipients.add(Address('smsold@gmail.com'))
      ..subject = subject
      ..text = "$recipeintEmail asked a question $msg";

    try {
      final sendReport = await send(message, smtpServer);
      print('Message sent: ' + sendReport.toString());
    } on MailerException catch (e) {
      print('Message not sent.');
      setState(() {
        this.showLoading = false;
      });

      for (var p in e.problems) {
        print('Problem: ${p.code}: ${p.msg}');
      }
    }
  }
}
