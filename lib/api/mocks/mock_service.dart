import 'package:realestateguru/api/mocks/models/question.dart';

import 'models/about.dart';

class MockService {
  List<Question> getAllQuestions() {
    return questions;
  }

  Question fetchQuestionById(int id) {
    Question question = questions.singleWhere((element) => element.id == id);
    return question;
  }

  About getAppABout() {
    return about;
  }
}
