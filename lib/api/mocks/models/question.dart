class Question {
  int id;
  String question;
  String answer;
  String datePublished;
  String publishedBy;
  String imagePath;
  Question(
      {this.id,
      this.question,
      this.imagePath,
      this.answer,
      this.datePublished,
      this.publishedBy});
}

List<Question> questions = [
  Question(
      id: 1,
      imagePath: 'assets/images/1.jpg',
      question:
          'If you were selling a home and received two offers, which one would you take? ',
      answer: """Answer: Know Thy Seller
Most sellers would opt for the offer with the strongest pre-qualification letter from a reputable lender. Unknown to most however, is that pre-qualification letters are not worth the paper they are written on.
Understanding what a savvy home seller (and their agent) will be subtly looking for during a negotiation puts you in the power position. Wary home sellers will be looking to analyze several key items on your lender letter:
1. FICO score
2. Proof of down payment and closing costs – liquid 
3. Sellers will look for a clause from the lender that says approved, subject to appraisal only. This shows that the buyer has taken the time to get vetted by his lender and is a serious candidate backed by a serious lender. A savvy seller will not accept an offer that does not have this key statement.
Now, put your buyer hat on. You’ve decided to buy a house. Most buyers get excited and start driving around and looking at open houses – followed by emotion-laden calls to an agent regarding properties they are interested in. WRONG! If you go out to a fancy dinner, you probably know the limit on your credit card. Same principle applies here. Meet with a reputable lender … first. Establish a price range you can afford …. and only then start shopping. It is embarrassing when your credit card doesn’t go through after a fine meal; likewise, imagine finding out (after the fact) that you really can’t afford your dream home. 
Remember, educate yourself, think like a seller, and you’ll buy at the best price possible for your new home. For more information, visit www.RealEstateGuru.com""",
      publishedBy: 'Devin Jackson',
      datePublished: "10/22/2020 10:10PM"),
  Question(
      id: 2,
      imagePath: 'assets/images/2.jpeg',
      question: 'Six tips for buying a vacation home?',
      answer:
          """    • Beware joint ventures: Ideally, one person or family will buy and use the home.
    • Consider renting it out: If you’re buying in a beach town and thinking of renting the home when you’re not there, get close to the water. Walking distance is most in demand
    • Figure on extra costs: Think about how you would handle the business of rentals, including whether to hire a management company.
    • Make it a stress-free trip: Many people prefer a relatively short trek to their second home, so buying something that’s between a 45-minute and a couple-of-hours’ drive from your primary residence can be a good idea.
    • Pick the right beach: Buying a house along the beach brings its own set of decisions.
    • Don’t isolate yourself: If it’s isolation you’re after, that’s fine. But be aware of the financial implications. """,
      publishedBy: 'Pierre Cox',
      datePublished: "10/08/2020 12:10PM"),
  Question(
      id: 3,
      imagePath: 'assets/images/1.jpg',
      question: 'What is a seller’s market?',
      answer:
          'In sellers’ markets, increasing demand for homes drives up prices. Here are some of the drivers of demand:Economic factors – the local labor market heats up, bringing an inflow of new residents and pushing up home prices before more inventory can be built.Interest rates trending downward – improves home affordability, creating more buyer interest, particularly for first time home buyers who can afford bigger homes as the cost of money goes lower.A short-term spike in interest rates - may compel “on the fence” buyers to make a purchase if they believe the upward trend will continue. Buyers want to make a move before their purchasing power (the amount they can borrow) gets eroded.Low inventory - fewer homes on the market because of a lack of new construction. Prices for existing homes may go up because there are fewer units available..',
      publishedBy: 'Alan Joseph',
      datePublished: "09/20/2020 08:00PM"),
  Question(
      id: 4,
      imagePath: 'assets/images/2.jpeg',
      question: 'What the first step of the home buying process?',
      answer:
          'Getting pre-approved for a mortgage is the first step of the home buying process. Getting a pre-approval letter from a lender get the ball rolling in the right direction.Here’s why First, you need to know how much you can borrow. Knowing how much home you can afford narrows down online home searching to suitable properties, thus no time is wasted considering homes that are not within your budget. (Pre-approvals also help prevent disappointment caused by falling in love unaffordable homes.Second, the loan estimate from your lender will show how much money is required for the down payment and closing costs. You may need more time to save up money, liquidate other assets or seek mortgage gift funds from family. In any case, you will have a clear picture of what is financially required.Finally, being pre-approved for a mortgage demonstrates that you are a serious buyer to both your real estate agent and the person selling their home.Most real estate agents will require a pre-approval before showing homes - this is especially true at the higher end of the real estate market; sellers of luxury homes will only allow pre-screened (and verified) buyers to view their homes. This is meant to keep out "Looky Lous" and protect the seller’s privacy. What’s more, by limiting who enters their home, sellers are given extra security from potential thieves trying to case the home (like identifying security systems, locating expensive artwork or other high-value personal property).',
      publishedBy: 'Thomas Crane',
      datePublished: "09/10/2020 04:10PM"),
  Question(
      id: 5,
      imagePath: 'assets/images/1.jpg',
      question: 'What is a stratified market?',
      answer:
          'A stratified market happens where supply and demand characteristics differ by price point, in the same area (typically by city). For example, home sales for properties above .5M may be brisk (seller’s market) while homes under  may be sluggish (buyer’s market). This scenario comes along every so often in West Coast cities where international investors - looking to park their money in the United States - buy expensive real estate. At the same time, home sales activity in mid-priced homes could be entirely different.',
      publishedBy: 'Alvaro Mcgee',
      datePublished: "08/22/2020 10:20PM"),
];
