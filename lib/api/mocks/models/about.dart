class About {
  String heading1;
  String heading2;
  String aboutSection1;
  String aboutSection2;
  About({this.heading1, this.heading2, this.aboutSection1, this.aboutSection2});
}

About about = About(
    heading1: "",
    heading2: "A Star Team Is Born",
    aboutSection1:
        """Phil Immel, Founder & CEO of the Immel Team, was bestowed the prestigious “legend” award for 15 years of top broker performance by Berkshire Hathaway Home Services.Licensed at age 19, while majoring in real estate & marketing at San Diego State University, Immel became a broker at 21 years old and received numerous accolades during his illustrious career. He eventually went into corporate management for Coldwell Banker and became it’s youngest vice president at age 28. After 10 years in management, he returned to sales in Orange County.Immel is also known as the Real Estate Guru ®.  The “Guru” is the spokesperson on national and local television & radio. His honest market assessment in these turbulent days has created many return guest spots on FOX, CNN and CBS Radio. Additionally, he is a contributing writer for the Wall Street Journal, Yahoo Finance & Reuters.""",
    aboutSection2:
        """Armed with 40 years of real estate experience, Phil started Immel Team Luxury Real Estate, a boutique firm that specializes in coastal properties in South Orange County. This small successful team has a big presence in the upscale neighborhoods of Dana Point.
This dynamic multi-general Team has a quality, not quantity approach to business. Agents proudly represent high-net worth clients and celebrities on the listing and buyer sides of transactions.
Our clients are entrepreneurs, entertainment personalities and executives for major corporations. Foundations and institutional clients include St. Joseph/Mission Hospital as well as Loma Linda University.
The Immel Team can be contacted at 949.249.2020, or visit our office at 10 Monarch Bay Plaza, Dana Point, CA 92629.""");
